from flask import Flask, request, session
from flask_cors import CORS, cross_origin
import json

from songs import Songs

#Start the API
app = Flask(__name__)
CORS(app)

@app.route('/songs', methods=['GET'])
def get_all_songs():
    skip = int(request.args.get('skip', 0))
    count = int(request.args.get('count', 20))

    songs_library = Songs(Songs.TEST_FILE)
    return songs_library.get_songs(skip, count)


@app.route('/songs/avg/difficulty', methods=['GET'])
def get_songs_average_difficulty():
    songs_library = Songs(Songs.TEST_FILE)
    return songs_library.get_average_difficulty()


@app.route('/songs/search', methods=['GET'])
def search_songs():
    phrase = request.args.get('phrase', '')
    songs_library = Songs(Songs.TEST_FILE)
    return songs_library.search_songs(phrase)

'''
    SESSION PERCISTENCE ISSUES
    I could not make the session object persist over different API calls.
    The meta posted through set_songs_meta won't be retrieved by get_songs_meta because the session object doesn't persist
'''
@app.route('/songs/<int:song_id>/meta', methods=['GET'])
def get_songs_meta(song_id):
    meta = session.get('songs_meta', {})
    if song_id in meta:
        return_data = meta[song_id]
    else:
        return_data = meta
    return json.dumps(return_data)

@app.route('/songs/<int:song_id>/meta', methods=['POST'])
def set_songs_meta(song_id):
    meta = session.get('songs_meta', {})
    meta[song_id] = request.json['meta']
    session['songs_meta'] = meta

    return json.dumps(meta)

app.secret_key = 'our secret!'
#app.config['SESSION_TYPE'] = 'filesystem'
app.run(port=5005, debug=True, threaded=True)