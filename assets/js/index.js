$(function()
{
	//Gets and displays the songs 
	$.ajax(
	{
	    url: 'http://127.0.0.1:5005/songs',
    	contentType: 'jsonp',

	    success: function(response)
	    {
	    	let songs = JSON.parse(response);

	    	//Sorts the songs by difficulty
	    	songs.sort(compareSongDifficulty);
	    	//Calculates and displays the total length of the all songs' titles
			$('#title-length').text("Combined length of all songs' titles: ");
			$('#title-length').append("<span class='badge'>" + calculateTitlesLength(songs) + "</span>");
	    	//Filters out difficulty < 10
	    	filterOutEasySongs(songs);
	    	//Calculates and displays the total length of the all displayed songs' titles
			$('#displayed-title-length').text("Combined length of all displayed songs' titles: ");
			$('#displayed-title-length').append("<span class='badge'>" + calculateTitlesLength(songs) + "</span>");
	    	
			//Displays songs in grid and badge mode
    		displayGrid(songs);
    		displayBadges(songs);
	    },
	    error: function(XMLHttpRequest, textStatus, errorThrown)
	    { 
        	alert("Status: " + textStatus + ", error: " + errorThrown + ", http: " + JSON.stringify(XMLHttpRequest));
        }
	});

	//Star badge
	var star = '<div id="bck1"><div id="bck2"><div id="bck3"><div id="bck4"><div id="star"></div></div></div></div></div>',
		new_col = '</td><td>';

	//Creates an entry in the table for each songs retrieved
	var displayGrid = function (songs_array)
	{
		let table_row = "";
		//Deletes every pre-existent entry in the table
		$("#song-table tbody").empty();

		$(songs_array).each(function(index)
		{
			//Prepares the grid row with current song data 
			table_row = "<tr><td>" + songs_array[index].artist + new_col + songs_array[index].title + new_col + songs_array[index].difficulty.toFixed(2) + new_col + songs_array[index].released  + new_col;
			//Adds a star for every rating unit
			for (var i = songs_array[index].rating; i > 0; i--)
			{
				table_row += star;
			}
			table_row += "<td><button id='" + songs_array[index].id + "' class='btn btn-primary meta-button'>Click</button></td></tr>";

			//Appends the row
			$('#song-table tbody').append(table_row);
		});
	};

	//Creates a badge for each songs retrieved
	var displayBadges = function (songs)
	{
		let rating = "",
			song = {};

		//Deletes every pre-existent badge
		$("#badge-container").html('');

		//Adds a <div> for every retrieved song
		$(songs).each(function(index) {
			jQuery('<div/>', {
			    class: 'song'
			}).appendTo("#badge-container");
		});

		//Fills the badge with song's data
		$(".song").each(function(index) {
			rating = "";
			song = songs[index];
			jQuery('<h3/>', {
				text: song.title
			}).appendTo(this);

			jQuery('<h4/>', {
				text: song.artist
			}).appendTo(this);

			jQuery('<p/>', {
				text: "Released: " + song.released
			}).addClass("released-date").appendTo(this);

			for (var i = song.rating; i > 0; i--)
			{
				rating += star;
			}
			jQuery(rating).appendTo(this);

			jQuery('<a/>', {
				'id': song.id,
				'text': 'Click'
			}).addClass("btn btn-primary btn-badge meta-button").appendTo(this);

			jQuery('<p/>', {
				text: "Difficulty"
			}).addClass("difficulty").appendTo(this);

			jQuery('<p/>', {
				text: song.difficulty.toFixed(2)
			}).appendTo(this);
		});
	};

	//Shows a nice message when no song has been retrieved
	var showNoSongs = function()
	{
		//Deletes every pre-existent entry in the table
		$("#song-table tbody").empty();
		//Deletes every pre-existent badge
		$("#badge-container").html('');

		let noSong = '<h4 style="text-align: center">No song to learn matches your creteria :(</h4>';
		table = "<tr><td colspan=6>" + noSong + "</td></tr>";

		$("#song-table tbody").append(table);
		$("#badge-container").append(noSong);
	}

	//Click listener for buttons
	$(document).on('click', '.meta-button', function()
	{
		//Makes a meta_data object as of given specifications
		var meta_data = {},
			clicked = {};
		clicked.clicked = true;
		meta_data.meta = clicked;
		let id = this.id,
			btn = $(this);

		//Calls the api to post metadata
		$.ajax(
		{
		    url: 'http://127.0.0.1:5005/songs/' + id + '/meta',
	    	type: "POST",
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
	    	data: JSON.stringify(meta_data),
		    success: function(response)
		    {
		    	//Gets metadata (won't work for session persistency issues in back-end)
		    	get_matadata(id);
		    	//Changes clicked button's style
		    	btn.removeClass('btn-primary').addClass('btn-success').text("Clicked")
		    },
		    error: function(XMLHttpRequest, textStatus, errorThrown)
		    { 
	        	alert("Status: " + textStatus + ", error: " + errorThrown + ", http: " + JSON.stringify(XMLHttpRequest));
	        }
		});
	});

	//Criteria to sort songs array
	var compareSongDifficulty = function (a, b)
	{
		if (a.difficulty > b.difficulty)
		{
			return -1;
		}
		if (a.difficulty < b.difficulty)
		{
			return 1;
		}
		return 0;
	}

	var get_matadata = function(id)
	{
		//Calls the api to get song's metadata
		$.ajax(
		{
		    url: 'http://127.0.0.1:5005/songs/' + id + '/meta',
	    	contentType: 'jsonp',

		    success: function(response)
		    {
		    	//Won't retrieve anything because the session object doesn't persist in the backend
		    	//alert(JSON.stringify(response));
		    },
		    error: function(XMLHttpRequest, textStatus, errorThrown)
		    { 
	        	console.log("Status: " + textStatus);
	        }
		});
	}

	var filterOutEasySongs = function (songs)
	{
		let cont = true,
    		splice_index = -1;
    	//The array is sorted by difficulty, so we iterate backwords
    	//and remove the songs until we find on with difficulty > 10 
    	for (var l = songs.length - 1; l >= 0 && cont; l--)
    	{
    		//If difficulty is > 10, we save the index (unlsess this is the first - hem...last - element)
    		//And flag that we need to exit the loop
    		if (songs[l].difficulty >= 10)
    		{
    			cont = false;
    			if (l != songs.length - 1)
    			{
    				splice_index = l + 1;
    			}
    		}
    	}

    	//If there are elements to filter out, we remove them
    	if (splice_index != -1)
    	{
    		songs.splice(splice_index, songs.length - splice_index);
    	}

    	return songs;
	}

	var calculateTitlesLength = function (songs)
	{
		let length = 0;
		for (var i = songs.length - 1; i >= 0; i--)
		{
			length += songs[i].title.length;
		}
		return length;
	}

	//Nav click listeners
	$('#grid-nav').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		$("#badges").hide();
		$("#grid").show();
	});
	$('#badges-nav').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		$("#grid").hide();
		$("#badges").show();
	});


	//Filters on every typing change in 
	$('#filter').on('input', function (e) {
		var phrase = $(this).val();
		$.ajax(
		{
		    url: 'http://127.0.0.1:5005/songs/search',
		    data: {'phrase': phrase},
	    	contentType: 'jsonp',

		    success: function(response)
		    {
		    	let songs = JSON.parse(response);

		    	//Sorts the songs by difficulty
		    	songs.sort(compareSongDifficulty);
		    	//Updates the total length of the all displayed songs' titles
				$('#displayed-title-length').text("Combined length of all displayed songs' titles: ");
				$('#displayed-title-length').append("<span class='badge'>" + calculateTitlesLength(songs) + "</span>");

				// If there are no songs, displays a nice message
				// Otherwise it shows the songs both in grid and in badges
		    	if (songs.length < 1)
		    	{
					showNoSongs();
				}
				else
				{
			    	displayGrid(songs);
			    	displayBadges(songs);
			    }
		    },
		    error: function(XMLHttpRequest, textStatus, errorThrown)
		    { 
	        	alert("Status: " + textStatus + ", error: " + errorThrown + ", http: " + JSON.stringify(XMLHttpRequest));
	        }
		});
	});
	
});